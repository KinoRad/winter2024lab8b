import java.util.Random;
public class Board{

    private Tile[][] board;
	Random rng;	
		
	public Board() {
		this.rng = new Random();
        this.board = new Tile[5][5];
        initializeBoard();
	}
	
	public void initializeBoard() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board[i][j] = Tile.Blank;
			}
		}
		hiddenWall();
	}

	public void hiddenWall(){
		for(int i = 0; i < board.length; i++){
			int randomeIndex = rng.nextInt(board[i].length);
			board[i][randomeIndex] =  Tile.HiddenWall;
		}
	}
	
	public Tile[][] getBoard(){
		return board;
	}
	
	public String toString(){
		String output = "This is the board: \n";
		for(Tile[] row : board){
			for(Tile tile : row){
				if(tile == Tile.Blank || tile == Tile.HiddenWall){
					output += Tile.Blank.getSymbol() + " ";
				}else if(tile == Tile.Wall){
					output += Tile.Wall.getSymbol() + " ";
				}else{
					output += Tile.Castle.getSymbol() + " ";
				}
			}
			output += "\n";
		}
		return output;
	}
	
	public int changeTile(int row, int colume){
		if(colume < 0 || colume >= board.length || row < 0 || row >= board.length){
			return -2;
		}else if(board[row][colume] == Tile.Castle || board[row][colume] == Tile.Wall){
			return -1;
		}else if(board[row][colume] == Tile.HiddenWall){
			board[row][colume] = Tile.Wall;
			return 1;
		}else{
			board[row][colume] = Tile.Castle;
			return 0;
		}
	}
	
	public boolean checkIfNotTaken(Tile t, int x, int y){
		if(board[y][x] == t){
			return false;
		}
		return true;
	}
	
}
