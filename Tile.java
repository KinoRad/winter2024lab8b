public enum Tile {
	Wall("W"),
	HiddenWall("_"),
    Blank("_"),
    Castle("C");

    private final String symbol;

    Tile(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}