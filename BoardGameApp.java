import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		
		Board b1 = new Board();
		System.out.println("Welcome to the game!");
		int numCastles = 5;
		int turns = 0;
		
		while(numCastles > 0 && turns < 8){
			System.out.println(b1);
			System.out.println("you have " + numCastles + " castles remaining");
			System.out.println("you have had " + turns + " turn(s)");
			System.out.println("Please enter the row you want (between 0 and 4): ");
			int row =  scanner.nextInt();
			System.out.println("Please enter the colume you want (between 0 and 4): ");
			int colume = scanner.nextInt();
			
			int move = b1.changeTile(row, colume);
			System.out.println("\n \n \n");
			
			
			if(move == -2 || move == -1){
				System.out.println("Please re-enter the colume and row");
			}else if(move == 1){
				System.out.println("there is a wall at that position");
				turns++;
			}else if(move == 0){
				System.out.println("the castle was successfully placed!");
				turns++;
				numCastles--;
			}
			System.out.println("\n \n \n");
		}
		System.out.println("This is the final board :");
		System.out.println(b1);
		
		if(numCastles == 0){
			System.out.println("You have won the game!");
		}else{
			System.out.println("LOL you lost... skill issue XD");
		}
	}
	
}